#Curso Udemy: Python para Data Science e Machine Learning

##Tricks Jupyter
* Shift+Enter: executa o código
* Alt+Enter: insere linha abaixo da linha selecionada
* Selecionando markdown no menu, você pode adicionar comentários
* Tab: Auto-complete
* Shift + Tab: mostra a descrição de um método

##Documentação Python 3
* https://docs.python.org/3/tutorial/index.html

##Numpy
* É uma biblioteca de álgebra linear para Python escrita em C
* Instalação da biblioteca pelo anaconda: $ conda install -c anaconda numpy
* Documentações: 
	* https://docs.scipy.org/doc/numpy/reference/ufuncs.html
	* https://docs.scipy.org/doc/numpy/user/quickstart.html	
	* https://docs.scipy.org/doc/numpy/reference/routines.html

##Pandas
* Instalação da biblioteca: $ conda install -c anaconda pandas
* Biblioteca escrita sobre o Numpy
* Permite rápida visualização e limpeza de dados
* Semelhante ao Excel
* Possui métodos próprios de visualização de dados
* Objetos:
	* Series é o objeto básico de Pandas
	* DataFrame é um conjunto de Series

##Matplotlib
* Instalação: $ conda install -c conda-forge matplotlib
* Biblioteca para visualização de dados
* Parecido com o MATLAB

##Seaborn
* Instalação: $ conda install -c anaconda seaborn 
* Biblioteca de visualização estatística de dados. Foi projetada para trabalhar com objetos DataFrame do pandas
* Site: https://seaborn.pydata.org/

##Machine Learning
* Livro texto: An Introduction to Statistical Learning with Applications in R
* Machine Learing é um método de análisde de dados que automatiza o precessão de criação de modelos. Permite que computadores encontrem padrões escondidos nos dados sem terem sido programados para isso.
* Algoritmos de Machine Learning:
	* Supervised Learning
	* Unsupervised Learning
	* Reinforcement Learning

##Scikit-learn
* Instalação: $ conda install -c anaconda scikit-learn
* Biblioteca para aprendizado de Machine Learning
